/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Geo
 */
public class Empleado {
    protected String puesto;
    protected float sueldo;
    
     Empleado(String puesto, float sueldo) {
             this.puesto = puesto;
             this.sueldo = sueldo;
     }
      public String getPuesto(){
         return puesto;
      }
      public float getSueldo(){
          return sueldo;
      }
      
      public String mostrarDatos(){
          return "Puesto: "+puesto+"\nSueldo: "+sueldo;
      }
     
    
}
