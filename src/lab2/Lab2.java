/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Geo
 */
public class Lab2 {

    
    public static void main(String[] args) {
       
       Empleado misEmpleados[]  = new Empleado[4];
       
       misEmpleados[0] = new Empleado("Gerente", 600);
       misEmpleados[1] = new Contador(2, "Finanzas", "Contador General", 700);
       misEmpleados[2] = new Ingeniero("Media", "Administrador de Redes", 1000);
       misEmpleados[3] = new Arquitecto("Planta de Producciones", "Arquitecto Master", 900);
       
       for(Empleado empleado: misEmpleados){
           System.out.println(empleado.mostrarDatos());
           System.out.println("");
       
       
           
       }
    }
    
}
