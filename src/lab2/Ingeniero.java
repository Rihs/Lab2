/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Geo
 */
public class Ingeniero extends Empleado{
    private String experiencia;
    
    
    public Ingeniero(String experiencia, String puesto, float sueldo){
        super(puesto, sueldo);
        this.experiencia = experiencia;
        
    }
    public String getExperiencia(){
        return experiencia;
        
    }
    @Override
    public String mostrarDatos(){
          return "Puesto: "+puesto+"\nSueldo: "+sueldo+"\n+Experiencia: "+experiencia;
    }
}
    
    

