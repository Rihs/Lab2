/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Geo
 */
public class Arquitecto extends Empleado{
    String planta;
    
    
    public Arquitecto (String planta, String puesto, float sueldo){
        super(puesto, sueldo);
        this.planta = planta;
        
    }
    
    public String getPlanta(){
        return planta;
    }
    
    @Override
    public String mostrarDatos(){
          return "Puesto: "+puesto+"\nSueldo: "+sueldo+"\nPlanta: "+planta;
    }
    
}
