/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Geo
 */
public class Contador extends Empleado{
    protected float anios_laborados;
    protected String departamento;
    
    public Contador (float anios_laborados, String departamento, String puesto, float sueldo ){
        super(puesto, sueldo);
        this.anios_laborados = anios_laborados;
        this.departamento = departamento;
    }
    
    public float getAnios_laborados(){
        return anios_laborados;
    }
    
    public String getDepartamento(){
        return departamento;
    }
    @Override
    public String mostrarDatos(){
          return "Puesto: "+puesto+"\nSueldo: "+sueldo+"\nAnios_laborados: "+anios_laborados+"\nDepartamento: "+departamento;
    
}
}